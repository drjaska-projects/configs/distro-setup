#!/bin/sh

set -eu

cat <<-EOF | sudo tee /etc/apt/sources.list
	deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
	deb-src http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware

	deb http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
	deb-src http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware

	deb http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware
	deb-src http://deb.debian.org/debian/ bookworm-updates main contrib non-free non-free-firmware
EOF

# do not warn when using non-free firmware
cat <<-EOF | sudo tee /etc/apt/apt.conf.d/no-nonfreefirmware-warn.conf
	APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";
EOF
