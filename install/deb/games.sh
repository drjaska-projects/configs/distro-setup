#!/bin/sh

set -eu

mkdir -p "$HOME/programs"
cd "$HOME/programs"

"$HOME/.config/distro-setup/install/common/heuristic/qstat.sh"

"$HOME/.config/distro-setup/install/common/heuristic/xqf.sh"

#"$HOME/.config/distro-setup/install/common/heuristic/visualboyadvance-m.sh"

"$HOME/.config/distro-setup/install/common/static/xonotic-morosophos-config.sh"

"$HOME/.config/distro-setup/install/common/static/xonotic-autobuild.sh"

"$HOME/.config/distro-setup/install/common/static/xonotic-0.8.2.sh"

"$HOME/.config/distro-setup/install/common/static/xonotic-0.8.5.sh"

"$HOME/.config/distro-setup/install/common/heuristic/xonotic-git.sh"
