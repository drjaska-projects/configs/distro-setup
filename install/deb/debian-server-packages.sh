#!/bin/bash

set -eu

# TRAILING - REMOVES THAT PACKAGE

declare -a PACKAGES
PACKAGES+=(
	# file receiving/sending
	curl
	wget
	rsync
	git
		git-extras
		git-lfs
		git-annex
	netcat-openbsd
	netcat-traditional-
	gpg

	# package manager
	#apt
		apt-file
		apt-listbugs
		apt-listchanges
		apt-transport-https
	aptitude

	# compiling
	clang
	gcc
	gdb
	mold
	make
	cmake
	automake
	autoconf
	build-essential
	pkgconf
	pkg-config
	meson
	sassc
	debhelper
	pandoc

	dos2unix

	# compression
	zip unzip
	tar
	p7zip-full
	#p7zip-rar # 13
	#unrar # 13

	# browsers
	lynx
	links
	amfora

	# python things
	python3
	python3-pip
	python3-venv
	python3-virtualenv
	python-is-python3

	# shells and interactivity
	zsh
		zsh-autosuggestions
		zsh-syntax-highlighting
		zsh-theme-powerlevel9k
	fish
	autojump
	grep
	ripgrep
	xclip
	xsel
	fzf
	retry
	bat
	tldr
	screen tmux
	command-not-found
	plocate
	reptyr
	whois
	fd-find
	entr
	psmisc
	pv
	moreutils # sponge
	debian-goodies
		popularity-contest-
	#which
	#man-db

	# tools
	tree
	jq
	nnn
		sshfs
	rclone
	visidata
		python3-openpyxl # xlsx support
		python3-xlrd # xls support

	# system information
	neofetch
	#fastfetch # 13
	#hyfetch # 13
	sysbench
	lshw
	procps # top
	htop
	btop
	powertop
	ncdu
	duf
	nethogs
	speedtest-cli
	socat
	smartmontools
	hdparm sdparm

	# editor stuff
	#vim-gtk3
	neovim
		python3-pynvim
		shellcheck

	# converters
	imagemagick
	ffmpeg

	# video/audio players
	vlc
	mpv
		mpv-mpris
	playerctl

	# media
	yt-dlp
	mkchromecast
	mkchromecast-alsa

	# Audio: Pipewire and pipewire-{pulse,alsa,jack}
	# on wireplumber instead of pw-media-session
	pipewire
	wireplumber
	pipewire-media-session-
	pipewire-alsa
	pipewire-pulse
	pipewire-jack
	libspa-0.2-jack
	pulseaudio-module-bluetooth-
	pulseaudio-utils
	avahi-daemon

	# Systemd
	systemd-timesyncd
	systemd-oomd

	# Firmware
	firmware-realtek
	firmware-linux
	firmware-linux-free
	firmware-linux-nonfree
)

yes | sudo apt-get update
yes | sudo apt-get upgrade
sudo apt-get install -y "${PACKAGES[@]}"

# pipewire stuff
sudo mkdir -p /etc/alsa/conf.d/
sudo mkdir -p /etc/ld.so.conf.d/

sudo ln -srf /usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-*.conf /etc/ld.so.conf.d/

# update tldr pages
tldr -u

# update default shell
if grep "$(whoami)" /etc/passwd | grep -vq '/zsh$'
then
	chsh -s "$(command -v zsh)"
fi

# symlink
# batcat to bat
# fdfind to fd
mkdir -p ~/.local/bin
ln -srf "$(command -v batcat)" ~/.local/bin/bat
ln -srf "$(command -v fdfind)" ~/.local/bin/fd
