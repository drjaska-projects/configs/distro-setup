#!/bin/sh

set -eu

# Default to Testing
cat <<-EOF | sudo tee /etc/apt/apt.conf.d/20-tum.conf
	APT::Default-Release "/^testing(|-security|-updates)$/";
EOF

# Set apt sources to Testing and Unstable, mixing them.
#
# Unstable repository packages can be installed with
#   sudo apt install -t unstable $packagename
#
# If a package doesn't exist in Testing then it'll be installed
# from Unstable without asking
#
# https://wiki.debian.org/DebianUnstable#Can_I_use_Sid_packages_on_.22testing.22.3F
cat <<-EOF | sudo tee /etc/apt/sources.list
	deb http://deb.debian.org/debian/ testing main contrib non-free non-free-firmware
	deb-src http://deb.debian.org/debian/ testing main contrib non-free non-free-firmware

	deb http://deb.debian.org/debian/ unstable main contrib non-free non-free-firmware
EOF

# Migration from pinning rules to default-release preference
[ -e /etc/apt/preferences.d/99pin-unstable ] && \
	sudo rm /etc/apt/preferences.d/99pin-unstable

# Do not warn when using non-free firmware
cat <<-EOF | sudo tee /etc/apt/apt.conf.d/no-nonfreefirmware-warn.conf
	APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";
EOF
