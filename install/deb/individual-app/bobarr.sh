#!/bin/sh

set -eu

sudo apt-get update

sudo apt-get install -y openvpn network-manager-openvpn-gnome resolvconf docker docker-compose apt-transport-https

mkdir -p "$HOME/programs/bobarr"
cd "$HOME/programs/bobarr"
curl -o- https://raw.githubusercontent.com/iam4x/bobarr/master/scripts/install.sh | bash
