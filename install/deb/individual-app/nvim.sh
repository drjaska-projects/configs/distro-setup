#!/bin/sh

# shellcheck disable=SC2317

set -eu

# Debian Testing finally updated their neovim
# install neovim via apt instead
exit 1

URL="https://github.com/neovim/neovim/releases/tag/stable"

if command -v nvim >/dev/null 2>&1
then
	echo "neovim already installed, checking for updates"
	VERSION="$(nvim --version | head --lines 1)"

	if curl "$URL" | grep -q "$VERSION"
	then
		echo "no neovim updates found"
		exit 0
	else
		echo "found neovim updates, reinstalling"

		sudo apt remove -y neovim

		# remove neovim-runtime in case of debian's packaged
		# neovim, not necessary for github releases as they
		# are a single package
		sudo apt autoremove -y
	fi
fi

cd /tmp/

wget "https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.tar.gz"

#sudo apt install -y ./nvim-linux64.deb

if ! [ -d "$HOME/.config/nvim" ]
then
	cd "$HOME/.config/"
	git clone "https://gitlab.com/drjaska-projects/configs/nvim.git"
else
	cd "$HOME/.config/nvim"
	git pull
fi

#rm ./nvim-linux64.tar.gz
