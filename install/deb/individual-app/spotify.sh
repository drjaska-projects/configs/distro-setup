#!/bin/sh

set -eu

# Spotify

curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
yes | sudo apt-get update --allow-insecure-repositories
yes | sudo apt-get install -y spotify-client
