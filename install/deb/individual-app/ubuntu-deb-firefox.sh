#!/bin/sh

set -eu

sudo snap remove --purge firefox #purge the snap firefox install
sudo apt-get purge -y firefox #purge the decoy which just links to the snap package

yes | sudo add-apt-repository ppa:mozillateam/ppa #add The PPA for .deb package which isn't a decoy

#create an apt preference file which gives the real .deb install priority over the decoy
cat <<EOF | sudo tee /etc/apt/preferences.d/mozillateamppa
Package: firefox*
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 501
EOF

sudo apt-get install -y --target-release 'o=LP-PPA-mozillateam' firefox #finally install the goodness
