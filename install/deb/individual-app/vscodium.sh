#!/bin/sh

# shellcheck disable=SC2317

set -eu

# DEBIAN HAS NO VAAPI SUPPORT FOR ELECTRON APPS
# `codium --disable-gpu` still fucking crashes
# fuck novideo GPUs and fuck this browser
# `sudo apt install nvidia-vaapi-driver` only fixes it for firefox and ffmpeg, not electron

exit 69

# yarn sorse
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo tee /etc/apt/trusted.gpg.d/yarn.asc
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# all of the dependencies
sudo apt install yarn nodejs jq git gcc make pkg-config libx11-dev libxkbfile-dev libsecret-1-dev fakeroot rpm libkrb5-dev

# pull or clone latest git
if [ -d "$HOME/programs/vscodium/.git" ]
then
	cd "$HOME/programs/vscodium"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/VSCodium/vscodium
	cd vscodium
fi

build/build.sh

# ln -srf VSCode-linux-x64/codium "$HOME/bin/codium"
