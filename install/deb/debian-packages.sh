#!/bin/bash

set -eu

GUI="${GUI-yes}"

# TRAILING - REMOVES THAT PACKAGE

declare -a PACKAGES
PACKAGES+=(
	# file receiving/sending
	curl
	wget
	rsync
	git
		git-extras
		git-lfs
		git-annex
	netcat-openbsd
	netcat-traditional-
	gpg

	# package manager
	#apt
		apt-file
		apt-listbugs
		apt-listchanges
		apt-transport-https
	aptitude

	# compiling
	clang
	gcc
	gdb
	mold
	make
	cmake
	automake
	autoconf
	build-essential
	pkgconf
	pkg-config
	meson
	sassc
	debhelper
	pandoc

	dos2unix

	# compression
	zip unzip
	tar
	p7zip-full
	p7zip-rar
	unrar

	# browsers
	lynx
	links
	amfora

	# python things
	python3
	python3-dev # keyszer dependency
	python3-pip
	python3-venv
	python3-virtualenv
	python-is-python3

	# shells and interactivity
	zsh
		zsh-autosuggestions
		zsh-syntax-highlighting
		zsh-theme-powerlevel9k
	fish
	autojump
	grep
	ripgrep
	xclip
	xsel
	fzf
	retry
	bat
	tldr
	screen tmux
	command-not-found
	plocate
	reptyr
	whois
	fd-find
	entr
	psmisc
	pv
	moreutils # sponge
	debian-goodies
		popularity-contest-
	gpm
	which
	man-db

	# tools
	tree
	jq
	nnn
		sshfs
	rclone
	visidata
		python3-openpyxl # xlsx support
		python3-xlrd # xls support

	# system information
	neofetch
	fastfetch
	hyfetch
	sysbench
	lshw
	procps # top
	htop
	btop
	powertop
	ncdu
	duf
	nethogs
	speedtest-cli
	socat
	smartmontools
	hdparm sdparm

	# editor stuff
	#vim-gtk3
	neovim
		python3-pynvim
		bear
		universal-ctags
		# packages for LSPs
		npm
		shellcheck
	ed
	indent

	# converters
	imagemagick
	ffmpeg

	# video/audio players
	vlc
	mpv
		mpv-mpris
	playerctl

	# media
	yt-dlp
	mkchromecast
	mkchromecast-alsa

	# Audio: Pipewire and pipewire-{pulse,alsa,jack}
	# on wireplumber instead of pw-media-session
	pipewire
	wireplumber
	pipewire-media-session-
	pipewire-alsa
	pipewire-pulse
	pipewire-jack
	libspa-0.2-jack
	pulseaudio-module-bluetooth-
	pulseaudio-utils
	avahi-daemon

	# dependencies for ?
	libffi-dev
	libffi8
	libgmp10
	libncurses-dev
	libncurses6
	libtinfo6

	# Fonts
	fonts-noto
		fonts-noto-color-emoji
	fonts-unifont
	fonts-opendyslexic
	ttf-mscorefonts-installer
	fontconfig

	# lolz
	cbonsai
	lolcat

	# Systemd
	systemd-timesyncd
	systemd-oomd

	# Virtual framebuffer
	xvfb

	# Firmware
	firmware-realtek
	firmware-linux
	firmware-linux-free
	firmware-linux-nonfree
	nvidia-detect
)

yes | sudo apt-get update
yes | sudo apt-get upgrade
sudo apt-get install -y "${PACKAGES[@]}"
PACKAGES=()

if [ "$GUI" = "yes" ]
then
	# Element
	sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg \
		https://packages.element.io/debian/element-io-archive-keyring.gpg
	echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" \
		| sudo tee /etc/apt/sources.list.d/element-io.list

	case "$(uname -m)" in
		x86_64)
			# Spotify
			wget -O- https://download.spotify.com/debian/pubkey_6224F9941A8AA6D1.gpg \
				| sudo gpg --dearmor --yes -o /etc/apt/trusted.gpg.d/spotify.gpg
			echo "deb http://repository.spotify.com stable non-free" \
				| sudo tee /etc/apt/sources.list.d/spotify.list

			PACKAGES+=( spotify-client )
			;;
	esac

	PACKAGES+=(
		# X11
		xorg
		xdotool
		x11-xserver-utils
		arandr
		#picom
		feh
		imv
		rofi

		# Xonotic dependencies
		libtool
		libgmp-dev
		libsdl2-dev
		libxpm-dev
		xserver-xorg-dev
		zlib1g-dev
		libjpeg62-turbo-dev # libjpeg-turbo8-dev on Ubuntu

		# dwm dependencies
		libx11-dev
		libxinerama-dev
		libxft-dev
		sharutils

		# XQF dependencies
		libxml2-dev
		libminizip-dev
		libreadline-dev
		libgeoip-dev
		libx11-dev
		zlib1g-dev
		libgtk2.0-dev
		intltool
		debhelper

		# xedgewarp dependencies
		libxcb-randr0-dev
		libxcb-util-dev
		libxcb1-dev
		libx11-xcb-dev
		libx11-dev
		libxi-dev

		# Partitioner
		gparted
			btrfs-progs # for btrfs partitions
			dosfstools # for FAT16 and FAT32 partitions
			mtools # utilities to access MS-DOS disks

		# Git GUIs
		meld
		gitk

		# Office, image, video and audio programs
		gimp
		krita
		inkscape
		libreoffice
		audacity
		obs-studio

		# Notifications
		dunst

		# File browser
		pcmanfm
		gtk2-engines-pixbuf
		gtk2-engines-murrine
		gnome-themes-extra
		ark file-roller

		# Disk usage
		filelight

		# Browsers
		#firefox # unstable installed later
		chromium

		# mirror phone screen to PC
		scrcpy

		# Game emulators
		retroarch mgba-qt desmume libretro-desmume

		# gnome keyring secret.service
		gnome-keyring
		seahorse

		# Terminals
		kitty
		konsole
		stterm
		gnome-terminal

		# Printer support
		cups-daemon

		# Gaming device configurer
		piper

		# Messaging
		element-desktop
		firejail

		# GNOME configurer
		dconf-cli

		# screenshots
		maim
		suckless-tools
	)
fi

# deprecated packages for my setup:
# xmonad stalonetray nautilus micro

# TODO: detect laptop to install:
# firmware-atheros xserver-xorg-input-mtrack

# avoid network manager on netboot PCs
# TODO: improve detection from a hack to something more exportable
if ! sudo [ -d /mnt/root/nfs ]
then
	PACKAGES+=(
		network-manager
		network-manager-gnome
		network-manager-openvpn
		network-manager-openvpn-gnome
		network-manager-ssh
		network-manager-ssh-gnome
		network-manager-config-connectivity-debian
	)
fi

yes | sudo apt-get update
yes | sudo apt-get upgrade
sudo apt-get install -y "${PACKAGES[@]}"

if lspci | grep VGA | grep -q NVIDIA
then
	while true
	do
		PACKAGE="$(nvidia-detect | grep -A 1 "It is recommended to install" \
			| tail -n 1 | sed -e 's# ##g' -e 's#\.##g')"

		[ -z "${PACKAGE-}" ] && break

		printf "Do you wish to install %s? (y/n): " "$PACKAGE"
		stty raw
		REPLY=$(dd bs=1 count=1 2> /dev/null)
		stty -raw
		echo ''
		case "$REPLY" in
			[Yy]|"$(printf '\r')"|"$(printf '\n')"|" ")
				case "$(uname -m)" in
					x86_64)
						sudo apt-get install -y linux-headers-amd64
						;;
					*)
						echo "TODO: install linux-headers-$(uname -m)"
						exit 1
						;;
				esac
				sudo apt-get install -y "$PACKAGE" nvidia-vaapi-driver
				break
				;;
			[Nn]|"$(printf '\e')")
				break
				;;
			*)
				printf '%s\n' 'Please answer y/enter/space or n/esc.'
				;;
		esac
	done
fi

# pipewire stuff
sudo mkdir -p /etc/alsa/conf.d/
sudo mkdir -p /etc/ld.so.conf.d/

# alsa conf is nowadays symlinked by default?
# is pipewire-jack conf too?
#sudo ln -srf /usr/share/doc/pipewire/examples/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d/
sudo ln -srf /usr/share/doc/pipewire/examples/ld.so.conf.d/pipewire-jack-*.conf /etc/ld.so.conf.d/

# dependant on unstable/testing repository, not in default repository at the time of writing
#sudo apt-get install -y haskell-stack audacity stalonetray nvidia-driver || true

#sudo apt-get install -t unstable -y firefox

# child shell script imcompatible due to a pop-up
#sudo apt-get install -y qjackctl

# update tldr pages
tldr -u

# update default shell
if grep "$(whoami)" /etc/passwd | grep -vq '/zsh$'
then
	chsh -s /usr/bin/zsh
fi

# symlink
# batcat to bat
# fdfind to fd
mkdir -p ~/.local/bin
ln -srf "$(command -v batcat)" ~/.local/bin/bat
ln -srf "$(command -v fdfind)" ~/.local/bin/fd

# recreate font caches
if [ -e "$HOME/.fontconfig" ]
then
	sudo fc-cache -f -v
else
	# why does fc-cache exit code 8 if ~/.fontconfig doesn't
	# exist if it doesn't need to place anything in it...
	mkdir -p "$HOME/.fontconfig"
	sudo fc-cache -f -v
	rmdir "$HOME/.fontconfig"
fi
