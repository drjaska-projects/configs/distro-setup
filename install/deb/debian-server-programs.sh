#!/bin/sh

set -eu

cd "$HOME"
mkdir -p programs
cd programs

git clone https://github.com/so-fancy/diff-so-fancy || true

"$HOME/.config/distro-setup/install/common/static/diff-so-fancy.sh"
