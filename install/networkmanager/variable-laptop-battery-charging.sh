#!/bin/bash

set -eu

##################
# <Yoink credit> #
##################
#
####################################################################
# https://gist.github.com/zouppen/755855a1044fd45a8260387f176be0eb #
####################################################################
#
###################
# </Yoink credit> #
###################

#############
# <Warning> #
#############
#
##################################################################################
# This setup script does not work with SSIDs which fuck with Regular Expressions #
##################################################################################
#
##############
# </Warning> #
##############

# Which battery do we control?
BATTERY="${BATTERY-"/sys/class/power_supply/BAT0"}"

# File to store the following snippet script for NetworkManager in
CFGFILE="${CFGFILE-"/etc/NetworkManager/dispatcher.d/limit-charge"}"

# Preference permanence, avoid resetting them
if ! [ -e "$CFGFILE" ]
then
	sudo mkdir -pv "$(dirname "$CFGFILE")"

	# Create the script which includes preferences
	cat <<-EOF | sudo tee "$CFGFILE"
		#!/bin/sh

		set -eu

		case "\${2-}" in
		    up|down)
		        # Avoid double bookkeeping which connections are up or down,
		        # so just check it here.
		        if nmcli --terse --fields name connection show --active | grep -qE '^Foonet$|^Barnet$'
		        then
		            # Home, avoid wearing of the battery
		            charge=80
		        else
		            # Away, maximum charge
		            charge=100
		        fi
		        echo "\$charge" > "$BATTERY/charge_control_end_threshold"
		        ;;
		esac
	EOF

	sudo chmod +x "$CFGFILE"
fi



# Fetch saved networks from connection history
networks=()
while read -r line
do
	networks+=("$line")
done <<< "$(nmcli -t -f name connection show)"

echo
echo "Networks found in connection history:"
echo "$(IFS=$'\n'; echo "${networks[*]}")"
echo
echo



for network in "${networks[@]}"
do
	# Does $network already exist in $CFGFILE?
	if grep -q "$network" "$CFGFILE"
	then
		while true
		do
			echo -n "Remove '$network' from home networks? y[es]/n[o]: "
			read -r answer

			case "$answer" in
				y*|Y*)
					# Double quotes eat a backslash from \ and from $ and thus \\\$ is \$
					sudo sed "s#\^${network}\\\$|##" -i "$CFGFILE"
					echo -n "Removed '$network' from home networks"
					echo
					break
					;;
				n*|N*)
					echo "Kept '$network' as a home network"
					echo
					break
					;;
				*)
					echo "Unidentified answer"
					;;
			esac
		done
	else
		while true
		do
			echo -n "Add '$network' to the list of home networks? y[es]/n[o]: "
			read -r answer

			case "$answer" in
				y*|Y*)
					# Double quotes eat a backslash from \ and from $ and thus \\\$ is \$
					sudo sed "s#\^Barnet\\\$#\^${network}\\\$|\^Barnet\\\$#" -i "$CFGFILE"
					echo "Set battery to not fully charge when connected to '$network'"
					echo
					break
					;;
				n*|N*)
					echo "Ignored '$network'"
					echo
					break
					;;
				*)
					echo "Unidentified answer"
					;;
			esac
		done
	fi
done
