#!/bin/bash

set -eu

# Which battery do we control?
BATTERY="${BATTERY-"/sys/class/power_supply/BAT0"}"

# File to store the following snippet script for NetworkManager in
CFGFILE="${CFGFILE-"/etc/NetworkManager/dispatcher.d/limit-charge"}"

sudo mkdir -pv "$(dirname "$CFGFILE")"

# Create the script which includes preferences
cat <<-EOF | sudo tee "$CFGFILE"
	#!/bin/sh

	set -eu

	echo "80" > "$BATTERY/charge_control_end_threshold"
EOF

sudo chmod +x "$CFGFILE"
