#!/bin/sh

set -eu

HERE="$(realpath "$(dirname "$0")")"

TMPFILE="$(mktemp)"

sudo cp "$HERE/pwfeedback" "$TMPFILE"
sudo chmod o-rwx "$TMPFILE"
sudo chown 0:0 "$TMPFILE"
sudo mv "$TMPFILE" /etc/sudoers.d/pwfeedback
