#!/bin/sh

set -eu

mkdir -p "$HOME/programs"
cd "$HOME/programs"

clonerepo() {
	if ! [ -d "$2" ]
	then
		git clone "$1"
	fi
}

clonerepo https://gitlab.com/drjaska-projects/dwm.git                dwm
clonerepo https://github.com/joshgoebel/keyszer.git                  keyszer
clonerepo https://github.com/sumnerevans/menu-calc.git               menu-calc
clonerepo https://git.suckless.org/quark                             quark
clonerepo https://gitlab.com/drjaska-projects/screensaver-matrix.git screensaver-matrix
clonerepo https://gitlab.com/drjaska-projects/slock.git              slock
clonerepo https://github.com/vifon/TrayCalendar.git                  TrayCalendar

"$HOME/.config/distro-setup/install/common/heuristic/dwm.sh"

"$HOME/.config/distro-setup/install/common/heuristic/keyszer.sh"

"$HOME/.config/distro-setup/install/common/heuristic/menu-calc.sh"

"$HOME/.config/distro-setup/install/common/heuristic/quark.sh"

"$HOME/.config/distro-setup/install/common/heuristic/screensaver-matrix.sh"

"$HOME/.config/distro-setup/install/common/heuristic/slock.sh"

"$HOME/.config/distro-setup/install/common/heuristic/traycalendar.sh"
