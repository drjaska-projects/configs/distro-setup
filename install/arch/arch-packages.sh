#!/bin/bash

set -eu



# check whether Chaotic AUR is installed or not
if ! pacman -Slq chaotic-aur > /dev/null
then
	"$HOME/.config/distro-setup/install/arch/chaotic-aur.sh"
fi



declare -a PACKAGES
PACKAGES+=(
	# file receiving/sending
	curl
	wget
	openssh
	mosh
	rsync
	git
		git-delta
		diff-so-fancy
		git-lfs
		git-annex
	openbsd-netcat
	gnupg

	# package manager
	pacman
		pacutils
		pacman-contrib
		pkgfile
		powerpill # Chaotic AUR
		paru # Chaotic AUR
		rate-mirrors # Chaotic AUR
		reflector
		reflector-simple # Chaotic AUR
		downgrade # Chaotic AUR
		informant # Chaotic AUR

	# compiling
	clang
	gcc
	gdb
	mold
	cmake
	pkgconf
	meson
	sassc
	base-devel
	rust
		cargo
		cargo-binstall
		cargo-update
		bacon
	pandoc-cli
		texlive-binextra
		texlive-latex
		texlive-latexextra
		texlive-plaingeneric
		texlive-fontsextra
		texlive-fontsrecommended

	dos2unix

	# compression
	lrzip
	lzip
	p7zip
	tar
	unace
	unarchiver
	unarj
	unrar
	zip unzip

	# connectivity
	dhclient
	networkmanager
		network-manager-applet
	traceroute
	nmap

	# browsers
	lynx
	links
	amfora

	# python things
	python
	python-pipenv

	# shells and interactivity
	zsh
		zsh-autosuggestions
		zsh-syntax-highlighting
		zsh-completions
		starship
	fish
		fish-autopair # Chaotic AUR
	bash
		bash-completion
	autojump # Chaotic AUR
	grep
	ripgrep-all
	ugrep
	xclip
	xsel
	wl-clipboard
	fzf
	retry
	bat
	sed
	sad # Space Age seD
	tealdeer
	cheat # Chaotic AUR
	screen tmux
	whois
	fd
	plocate
	reptyr
	entr
	pv
	moreutils # sponge
	gpm
	tesseract
		tesseract-data-eng
		tesseract-data-fin
	which
	man-db
	man-pages
	xdg-user-dirs
	find-the-command-git # Chaotic AUR

	# tools
	tree
	jq
	nnn
		sshfs
	rclone
	visidata
		python-openpyxl # xlsx support
		python-xlrd # xls support
		python-xlwt # xls support

	# system information
	neofetch
	fastfetch
	hyfetch
	sysbench
	lshw
	procps-ng # top
	htop
	btop
	powertop
	ncdu
	duf
	nethogs
	speedtest-cli
	socat
	smartmontools
	hdparm sdparm
	ethtool
	hblock
	hwinfo
	fwupd
	inxi
		bind          # dig: -i wlan IP
		binutils      # strings: -I sysvinit version
	#	curl          # -i (if no dig); -w,-W; -U
		dmidecode     # -M if no sys machine data; -m
		file          # -o unmounted file system (if no lsblk)
		hddtemp       # -Dx show hdd temp, if no drivetemp module
		iproute2      # ip: -i ip LAN
		kmod          # modinfo: Ax; -Nx module version
		lm_sensors    # sensors: -s sensors output
		lvm2          # lvs: -L LVM data
		mdadm         # -Ra advanced mdraid data
		mesa-utils    # glxinfo: -G (X) glx info
	#	smartmontools # smartctl: -Da advanced data
		upower        # -sx attached device battery info
		usbutils      # lsusb: -A usb audio; -J (optional); -N usb networking
		vulkan-tools  # vulkaninfo: -G vulkan info
	#	xorg-xdpyinfo # xdpyinfo: -G (X) Screen resolution, dpi; -Ga Screen size
	#	xorg-xdriinfo # xdriinfo: -G (X) DRI driver (if missing, fallback to Xorg log)
	#	xorg-xprop    # xprop: -S (X) desktop data
	#	xorg-xrandr   # xrandr: -G (X) monitors(s) resolution; -Ga monitor data

	# editor stuff
	#gvim
	neovim
		python-pynvim
		tree-sitter-cli
		ctags
		bear
		nodejs
			npm
		shellcheck
	ed
	indent

	# media
	vlc
	mpv
		mpv-mpris
	playerctl
	yt-dlp
		yt-dlp-drop-in
	# mkchromecast

	# Audio
	pipewire
	wireplumber
	pipewire-alsa
	pipewire-pulse
	pipewire-zeroconf
	pipewire-jack
	pipewire-v4l2
	libpulse # pactl
	avahi
	qjackctl
	ncspot
	whipper
	picard

	# Fonts
	otf-opendyslexic-nerd
	fontconfig

	# Yoink Garuda's fonts for now
	noto-fonts
		noto-fonts-extra
		noto-fonts-emoji # not on Garuda
	opendesktop-fonts
	otf-ipafont # Japanese
	ttf-anonymous-pro
	ttf-baekmuk # Korean
	ttf-caladea
	ttf-croscore
	ttf-dejavu
	ttf-fantasque-sans-mono
	ttf-fira-mono
	ttf-fira-sans
	ttf-firacode-nerd
	#ttf-hack
	ttf-hannom # Chinese and Vietnamese
	ttf-inconsolata
	ttf-indic-otf
	ttf-khmer
	ttf-lato
	ttf-liberation
	ttf-opensans
	ttf-roboto
	ttf-roboto-mono
	ttf-tibetan-machine # Tibetan
		xorg-mkfontscale # Unlisted dependency, mkfontscale & mkfontdir
	ttf-ubuntu-font-family
	fonts-tlwg # Thai
	#ttf-google-fonts-git # changes checkmark to cucumber emoji in kitty..?
	ttf-merriweather
	ttf-merriweather-sans
	ttf-oswald
	ttf-quintessential
	ttf-signika

	# lolz
	lolcat

	# Systemd
	systemd-oomd-defaults
	zram-generator

	# Firmware
	linux-firmware
)

if [ -d /boot/grub ]
then
	PACKAGES+=(
		update-grub
		os-prober-btrfs # Chaotic AUR
	)

	# Snapper for btrfs+grub
	if findmnt / -t btrfs &> /dev/null
	then
		PACKAGES+=(
			#btrfsmaintenance
			grub-btrfs
			snapper
				snap-pac
					snap-pac-grub # Chaotic AUR
					snapper-support # Chaotic AUR
		)
	fi
fi

# GUI

PACKAGES+=(
	# X11 environment
	xorg-server
	xorg-server-xvfb
	xorg-setxkbmap
	xorg-xauth
	brightnessctl #xorg-xbacklight
	xorg-xdpyinfo
	xorg-xdriinfo
	xorg-xev
	xorg-xhost
	xorg-xinit
	xorg-xinput
	xorg-xkill
	xorg-xmessage
	xorg-xprop
	xorg-xrandr autorandr
	xorg-xset
	xorg-xsetroot
	xsettingsd
	arandr
	feh
	rofi
	xdotool
	xmobar

	# Theming
	qt5ct qt6ct

	# Partitions / filesystems
	gparted
		# most are preinstalled
		btrfs-progs # for btrfs partitions
		dosfstools # for FAT16 and FAT32 partitions
		exfatprogs # for exFAT partitions
		ntfs-3g # for ntfs partitions
		mtools # utilities to access MS-DOS disks
	nfs-utils

	# Git GUIs
	meld

	# Office, image, video and audio programs
	gimp
	krita
	inkscape
	libreoffice-fresh
	audacity
	obs-studio
	spotify #Chaotir AUR
	maim
	spectacle
	cups
		cups-pdf
		libusb
		ipp-usb

	# Notifications
	dunst
	yad
	zenity

	# File stuff
	pcmanfm
	ark
	filelight

	# Browsers
	firefox
	floorp # Chaotic AUR
	librewolf # Chaotic AUR
	#ungoogled-chromium # Chaotic AUR

	scrcpy

	bonzomatic

	# Terminals
	kitty
	st # Chaotic AUR

	# Messaging
	element-desktop
	discord
		firejail
		betterdiscordctl-git # Chaotic AUR

	yaru-gtk-theme # Chaotic AUR

	# Laptop power profiles
	#auto-cpufreq # Chaotic AUR
		# low idle power but no throttling
	#power-profiles-daemon
		# several profiles

	vulkan-intel
	vulkan-mesa-layers
	vulkan-radeon
	vulkan-swrast
	vulkan-tools
	vdpauinfo
	usb_modeswitch
)

sudo pacman -S --needed "${PACKAGES[@]}"

tldr --update

sudo pacman -Fy
