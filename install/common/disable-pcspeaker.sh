#!/bin/sh

set -eu

# This disables a pc speaker from beeping and making noise

sudo mkdir -p /etc/modprobe.d/
cat <<-EOF | sudo tee /etc/modprobe.d/blacklist.conf
	blacklist pcspkr
EOF
