#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y \
			python3-gi \
			gobject-introspection \
			libgirepository-1.0-dev
		;;
	#garuda|arch)
		#;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

mkdir -p "$HOME/programs/cast-control"

python3 -m venv "$HOME/programs/cast-control/.venv"

. "$HOME/programs/cast-control/.venv/bin/activate"

python3 -m pip install -U cast-control

# Note: Why is the binary of cast-control with an underscore instead of a dash ...
ln -srf "$HOME/programs/cast-control/.venv/bin/cast_control" "$HOME/.local/bin/cast-control"
