#!/bin/sh

set -eu

if [ -d "$HOME/programs/xedgewarp/.git" ]
then
	cd "$HOME/programs/xedgewarp"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/Airblader/xedgewarp
fi

case "${distro-}" in
	debian)
		sudo apt-get install -y libxcb-randr0-dev libxcb-util-dev libxcb1-dev libx11-xcb-dev libx11-dev libxi-dev

		sudo apt-get install -y --no-install-recommends asciidoc
		cd "$HOME/programs/xedgewarp"
		make
		sudo make install
		make clean

		# cleanup compilation dependencies
		sudo apt-get remove -y asciidoc
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac
