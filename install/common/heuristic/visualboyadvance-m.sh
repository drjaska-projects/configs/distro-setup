#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		if [ -d "$HOME/programs/visualboyadvance-m/.git" ]
		then
			cd "$HOME/programs/visualboyadvance-m"
			git pull --autostash
		else
			mkdir -p "$HOME/programs"
			cd "$HOME/programs"
			git clone https://github.com/visualboyadvance-m/visualboyadvance-m.git
			cd visualboyadvance-m
		fi

		mkdir -p "$HOME/programs/visualboyadvance-m/build"
		cd "$HOME/programs/visualboyadvance-m"
		./installdeps
		cd "$HOME/programs/visualboyadvance-m/build"
		cmake .. -DBUILD_TESTING=OFF -G Ninja
		ninja

		ln -srf "$HOME/programs/visualboyadvance-m/build/visualboyadvance-m" "$HOME/.local/bin/visualboyadvance-m"
		;;
	garuda|arch)
		sudo pacman -S --needed vbam-sdl
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac
