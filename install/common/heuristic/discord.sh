#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y firejail
		mkdir -pv "$HOME/Downloads/"
		cd "$HOME/Downloads/"
		wget 'https://discord.com/api/download?platform=linux&format=deb' -O discord.deb

		sudo dpkg --install ./discord.deb || true
		yes | sudo apt-get install -y -f # installs missing dependencies
		sleep 3
		rm "$HOME/Downloads/discord.deb"

		curl -O https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl
		chmod +x betterdiscordctl
		mkdir -p "$HOME/.local/bin"
		mv betterdiscordctl "$HOME/.local/bin/"
		;;
	garuda|arch)
		sudo pacman -S --needed firejail discord betterdiscordctl
		;;
esac

# generate discord's config files and inject them
timeout 60 firejail --nodbus discord || true # will be killed and thus errors

betterdiscordctl install   || true
betterdiscordctl reinstall || true
