#!/bin/sh

set -eu

# If XORGLESS is set to false then keymapper's keyszer
# depends on having access to someone's :0 display
# the following command gives that access when ran in :0
# xhost +SI:localuser:keymapper

XORGLESS="${XORGLESS-true}"
USEACL="${USEACL-false}"

case "${distro-}" in
	debian)
		if [ "$XORGLESS" = "true" ]
		then
			if [ "$USEACL" = "false" ]
			then
				sudo apt-get install -y python3-dev python3-virtualenv python3-pip xvfb
			else
				sudo apt-get install -y python3-dev python3-virtualenv python3-pip acl xvfb
			fi
		else
			if [ "$USEACL" = "false" ]
			then
				sudo apt-get install -y python3-dev python3-virtualenv python3-pip
			else
				sudo apt-get install -y python3-dev python3-virtualenv python3-pip acl
			fi
		fi
		;;
	garuda|arch)
		if [ "$XORGLESS" = "true" ]
		then
			if [ "$USEACL" = "false" ]
			then
				sudo pacman -S --needed python-pipenv xorg-server-xvfb
			else
				sudo pacman -S --needed python-pipenv acl xorg-server-xvfb
			fi
		else
			if [ "$USEACL" = "false" ]
			then
				sudo pacman -S --needed python-pipenv
			else
				sudo pacman -S --needed python-pipenv acl
			fi
		fi
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

# check that we have a config to run
if ! [ -e "/usr/local/etc/keyszer/config.py" ]
then
	printf '\n\n%s\n\n\s\n\n' \
		"ERROR: KEYSZER REQUIRES A CONFIG TO RUN" \
		"PLEASE CREATE FILE /usr/local/etc/keyszer/config.py"
	exit 1
fi

# ensure that groups input and uinput exist
if ! getent group input
then
	sudo groupadd input
fi
if ! getent group uinput
then
	sudo groupadd uinput
fi
if ! getent group keymapper
then
	sudo groupadd keymapper
fi

# ensure that the user called keymapper exists
if [ -z "$(id keymapper)" ]
then
	sudo useradd --system --create-home -g keymapper \
		--shell /usr/bin/sh keymapper
	echo "created new user - keymapper"
fi

# ensure that keymapper is in input and uinput groups
if groups keymapper | grep -qv "input"
then
	sudo usermod -aG input keymapper
fi
if groups keymapper | grep -qv "uinput"
then
	sudo usermod -aG uinput keymapper
fi

# ensure that keymapper's homedir exists
sudo mkdir -p /home/keymapper
sudo chown -R keymapper:keymapper /home/keymapper
sudo chmod 700 /home/keymapper

# have keymapper run keyszer installation
installscript="$(mktemp)"
cp "$(dirname "$0")/keyszer-keymapper-installation.sh" "$installscript"
chmod 555 "$installscript"
sudo su - keymapper -c "$installscript"

# udev rules for allowing keymapper to access uinput
sudo mkdir -p /etc/udev/rules.d/
if [ "$USEACL" = "false" ]
then
	cat <<-EOF | sudo tee /etc/udev/rules.d/90-keymapper.rules
	KERNEL=="uinput", GROUP="uinput", MODE="0660", OPTIONS+="static_node=uinput"
	KERNEL=="event[0-9]*", GROUP="uinput", MODE="0660"
	EOF
else
	cat <<-EOF | sudo tee /etc/udev/rules.d/90-keymapper.rules
	KERNEL=="event*", SUBSYSTEM=="input", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/input/%k"
	KERNEL=="uinput", SUBSYSTEM=="misc", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/uinput"
	EOF
fi

# load said udev rules on boot
sudo mkdir -p /etc/modules-load.d/
cat <<EOF | sudo tee /etc/modules-load.d/uinput.conf
uinput
EOF

# either install a service which starts a virtual headless
# framebuffer for keyszer to attach to or install a service
# which depends on :0 display granting it access to it
# XORGLESS = true -> framebuffer, false -> depend on :0
sudo mkdir -p /usr/lib/systemd/system/
if [ "$XORGLESS" = "true" ]
then
	# systemd system service for running keyszer as an
	# unprivileged keymapper user which creates a virtual
	# headless framebuffer. Note that xvfb's argument is
	# \$DISPLAY here, it will be $DISPLAY in the service file
	cat <<-EOF | sudo tee /usr/lib/systemd/system/keyszer.service
	[Unit]
	Description=keyszer

	[Service]
	ExecStart=sh -c '/usr/bin/Xvfb \$DISPLAY -screen 0 1x1x24 & exec /home/keymapper/bin/keyszer -w -c /usr/local/etc/keyszer/config.py'
	Restart=on-failure
	RestartSec=3
	Environment=DISPLAY=:69
	User=keymapper

	[Install]
	WantedBy=graphical.target
	EOF
else
	# systemd system service for running keyszer
	# as an unprivileged keymapper user which depends
	# on having access to someone else's :0 display.
	cat <<-EOF | sudo tee /usr/lib/systemd/system/keyszer.service
	[Unit]
	Description=keyszer

	[Service]
	# Not running a virtual headless framebuffer with xvfb requires
	# any actual user to run an X11 server with the right DISPLAY
	# and to give keymapper access to it with the below cmd
	# /usr/bin/xhost +SI:localuser:keymapper

	ExecStart=/home/keymapper/bin/keyszer -w -c /usr/local/etc/keyszer/config.py
	Restart=on-failure
	RestartSec=3
	Environment=DISPLAY=:0
	User=keymapper

	[Install]
	WantedBy=graphical.target
	EOF
fi

# start keyszer
# load uinput for this boot so that reboot isn't needed
sudo modprobe uinput
# load, enable and start the systemd service
sudo systemctl daemon-reload
sudo systemctl enable keyszer.service
sudo systemctl start keyszer.service
