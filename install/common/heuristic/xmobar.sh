#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		if true # apt package over compilation
		then
			sudo apt-get install -y xmobar
		else
			sudo apt-get install -y haskell-stack

			if [ -d "$HOME/programs/xmobar/.git" ]
			then
				cd "$HOME/programs/xmobar"
				git pull --autostash
			else
				mkdir -p "$HOME/programs"
				cd "$HOME/programs"
				git clone https://codeberg.org/xmobar/xmobar.git
				cd xmobar
			fi

			stack install --flag xmobar:all_extensions
		fi
		;;
	garuda|arch)
		sudo pacman -S --needed xmobar
		;;
	*)
		echo "$0: unknown distro"
		exit 1
esac
