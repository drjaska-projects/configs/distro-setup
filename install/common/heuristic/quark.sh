#!/bin/sh

set -eu

if [ -d "$HOME/programs/quark/.git" ]
then
	cd "$HOME/programs/quark"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://git.suckless.org/quark
	cd quark
fi

sudo make clean install
