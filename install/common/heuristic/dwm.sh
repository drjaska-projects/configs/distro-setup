#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y build-essential libx11-dev libxinerama-dev sharutils suckless-tools libxft-dev
		;;
	garuda|arch)
		sudo pacman -S --needed libx11 libxinerama sharutils libxft
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/dwm/.git" ]
then
	cd "$HOME/programs/dwm"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://gitlab.com/drjaska-projects/dwm.git
	cd dwm
fi

sudo make clean install

case "${distro-}" in
	debian)
		sudo update-alternatives --install /usr/bin/x-session-manager x-session-manager /usr/local/bin/dwm 10
		sudo update-alternatives --install /usr/bin/x-window-manager x-window-manager /usr/local/bin/dwm 10
		;;
	# Arch-based distros don't seem to have anything like alternatives, skip
esac

sudo mkdir -p /usr/share/xsessions/
sudo mkdir -p /usr/share/icons/
sudo cp "$HOME/programs/dwm/dwm.desktop" /usr/share/xsessions/dwm.desktop
sudo cp "$HOME/programs/dwm/dwm.png" /usr/share/icons/dwm.png

make clean
