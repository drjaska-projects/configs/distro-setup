#!/bin/sh

set -eu

case "${distro-}" in
	ubuntu)
		sudo apt-get install -y autoconf automake build-essential curl libtool libgmp-dev libjpeg-turbo8-dev libsdl2-dev libxpm-dev xserver-xorg-dev zlib1g-dev unzip zip
		;;
	debian)
		sudo apt-get install -y autoconf automake build-essential curl libtool libgmp-dev libjpeg62-turbo-dev libsdl2-dev libxpm-dev xserver-xorg-dev zlib1g-dev unzip zip
		;;
	garuda|arch)
		sudo pacman -S --needed alsa-lib curl git libjpeg-turbo libmodplug libpng libvorbis libxpm xorgproto libxxf86vm sdl2 unzip zip
		;;
	*)
		echo "$0: unknown distro for Xonotic Git dependencies, exiting..."
		exit 1
		;;
esac

if [ -d "$HOME/xonotic/" ]
then
	XONOTICGIT="$HOME/xonotic/git"
else
	XONOTICGIT="$HOME/xonotic-git"
fi

# pull or clone latest git
if [ -d "$XONOTICGIT/.git" ]
then
	cd "$XONOTICGIT"
	git pull --autostash
else
	git clone https://gitlab.com/xonotic/xonotic.git "$XONOTICGIT"
	cd "$XONOTICGIT"
fi

./all update -l best
./all compile

ln -srf data/xonotic-data.pk3dir xd
