#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y build-essential libx11-dev
		;;
	garuda|arch)
		sudo pacman -S --needed libx11
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/slock/.git" ]
then
	cd "$HOME/programs/slock"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://gitlab.com/drjaska-projects/slock.git
	cd slock
fi

sudo make clean install
make clean
