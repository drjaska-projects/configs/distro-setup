#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y wget git python3 python3-venv
		;;
	# Red Hat-based:
	#sudo dnf install wget git python3
	# Arch-based:
	#sudo pacman -S --needed wget git python3
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

mkdir -p "$HOME/programs"

cd "$HOME/programs"

# TODO: add a pager audit stop here...
exec wget -qO- https://raw.githubusercontent.com/AUTOMATIC1111/stable-diffusion-webui/master/webui.sh | bash
