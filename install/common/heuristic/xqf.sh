#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y libxml2-dev libminizip-dev libreadline-dev libgeoip-dev libx11-dev zlib1g-dev libgtk2.0-dev intltool debhelper
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/xqf/.git" ]
then
	cd "$HOME/programs/xqf"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/XQF/xqf
fi

mkdir -p "$HOME/programs/xqf/build"
cd "$HOME/programs/xqf/build"
#cmake ..
cmake -DWITH_QSTAT=/usr/bin/qstat -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install
make clean
