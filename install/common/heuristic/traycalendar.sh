#!/bin/sh

set -eu

case "${distro-}" in
	#debian)
	#	;;
	garuda|arch)
		sudo pacman -S --needed python-gobject
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

cd "$HOME/programs"

if [ -d "TrayCalendar/.git" ]
then
	cd TrayCalendar
	git pull --autostash
else
	if [ -e "TrayCalendar" ]
	then
		rm -fr TrayCalendar
	fi
	git clone https://github.com/vifon/TrayCalendar.git
fi
