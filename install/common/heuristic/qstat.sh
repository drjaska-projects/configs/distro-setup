#!/bin/sh

set -eu

if [ -d "$HOME/programs/qstat/.git" ]
then
	cd "$HOME/programs/qstat"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/Unity-Technologies/qstat
	cd qstat
fi

exit 1
# FIXME: fix compilation and don't rely on release binaries

#wget https://github.com/Unity-Technologies/qstat/releases/download/v2.17/release.zip
#unzip release.zip -d "$HOME/programs/qstat"
#rm release.zip
#chmod a+x "$HOME/programs/qstat/bin/linux_amd64/qstat"

#sudo cp "$HOME/programs/qstat/bin/linux_amd64/qstat" /usr/bin/qstat
#sudo cp "$HOME/programs/qstat/bin/linux_amd64/qstat" /usr/bin/quakestat
