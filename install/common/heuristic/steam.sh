#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y flatpak gnome-software-plugin-flatpak
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install flathub com.valvesoftware.Steam

# flatpak run com.valvesoftware.Steam
