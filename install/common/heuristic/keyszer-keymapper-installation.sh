#!/bin/sh

set -eu

if [ -d "$HOME/programs/keyszer/.git" ]
then
	cd "$HOME/programs/keyszer"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/joshgoebel/keyszer
	cd keyszer
fi

python3 -m venv .venv
. .venv/bin/activate
pip3 install -e .

mkdir -p "$HOME/bin"
ln -srf "$HOME/programs/keyszer/.venv/bin/keyszer" "$HOME/bin/keyszer"
