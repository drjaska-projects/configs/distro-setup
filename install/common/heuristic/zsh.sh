#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y zsh
		;;
	garuda|arch)
		sudo pacman -S --needed zsh
		;;
esac

# update default shell
chsh -s /bin/zsh "$(whoami)"

if [ -e "$HOME/.config/zsh/plugins/install-plugins.sh" ]
then
	"$HOME/.config/zsh/plugins/install-plugins.sh"
else
	echo "could not find zsh plugin configs to install"
fi

if [ -e "$HOME/.config/zsh/sync-configs.sh" ]
then
	"$HOME/.config/zsh/sync-configs.sh"
else
	echo "could not find zsh configs to hook up"
fi
