#!/bin/sh

set -eu

if [ -d "$HOME/programs/farbfeld/.git" ]
then
	cd "$HOME/programs/farbfeld"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://git.suckless.org/farbfeld
	cd farbfeld
fi

sudo make clean install

if [ -d "$HOME/programs/sent/.git" ]
then
	cd "$HOME/programs/sent"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://git.suckless.org/sent
	cd sent
fi

sudo make clean install
