#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y bc xclip rofi
		;;
	garuda|arch)
		sudo pacman -S --needed bc xclip rofi
		;;
	*)
		"$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/menu-calc/.git" ]
then
	cd "$HOME/programs/menu-calc"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/sumnerevans/menu-calc
	cd menu-calc
fi

sudo make install
