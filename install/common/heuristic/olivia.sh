#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y libqt5webkit5-dev libtag1-dev socat mpv coreutils
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/olivia/.git" ]
then
	cd "$HOME/programs/olivia"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/keshavbhatt/olivia
fi

cd "$HOME/programs/olivia/src"
qmake Olivia.pro PREFIX="/usr" -spec linux-g++ CONFIG+=release
make
