#!/bin/sh

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

case "${distro-}" in
	debian)
		sudo apt-get install -y build-essential libxcb1-dev libxcb-render0-dev libxcb-shape0-dev libxcb-xfixes0-dev

		cargo install --locked --features clipboard broot
		;;
	garuda|arch)
		sudo pacman -S --needed broot
		;;
esac
