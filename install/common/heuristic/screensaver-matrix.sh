#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		sudo apt-get install -y ncurses-base
		;;
	garuda|arch)
		sudo pacman -S --needed ncurses
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

if [ -d "$HOME/programs/screensaver-matrix/.git" ]
then
	cd "$HOME/programs/screensaver-matrix"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://gitlab.com/drjaska-projects/screensaver-matrix.git
	cd screensaver-matrix
fi

make clean install
make clean
