#!/bin/sh

set -eu

mkdir -p "$HOME/programs/markdown_live_preview"

python3 -m venv "$HOME/programs/markdown_live_preview/.venv"
. "$HOME/programs/markdown_live_preview/bin/activate"

pip3 install --upgrade markdown_live_preview

ln -srf "$HOME/programs/markdown_live_preview/bin/mlp" "$HOME/.local/bin/mlp"
