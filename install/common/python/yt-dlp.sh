#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed yt-dlp
		exit 0
		;;
esac

mkdir -p "$HOME/programs/yt-dlp"

python3 -m venv "$HOME/programs/yt-dlp/.venv"

. "$HOME/programs/yt-dlp/.venv/bin/activate"

python3 -m pip install -U yt-dlp

ln -srf "$HOME/programs/yt-dlp/.venv/bin/yt-dlp" "$HOME/.local/bin/yt-dlp"
ln -srf "$HOME/programs/yt-dlp/.venv/bin/yt-dlp" "$HOME/.local/bin/youtube-dl"
