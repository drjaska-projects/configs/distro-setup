#!/bin/sh

set -eu

if [ -e "$HOME/quake3" ]
then
	echo "$0: Warning: ~/quake3 is not empty, aborting"
	exit 1
fi

TEMPFOLDER="$(mktemp -d)"

echo
echo "$TEMPFOLDER"
echo

cd "$TEMPFOLDER"

#wget --content-disposition 'https://dl.defrag.racing/downloads/game-bundles/DeFRaG%20Bundle%20all-in-one%20Linux%2064bit.7z'
curl -L 'https://dl.defrag.racing/downloads/game-bundles/DeFRaG%20Bundle%20all-in-one%20Linux%2064bit.7z' > response

case "$(file response)" in
	"response: 7-zip"*)
		mv response response.7z
		7z x "$TEMPFOLDER/response.7z" -o"$TEMPFOLDER/quake3"
		;;
	*)
		echo "$0: unknown response filetype: $(file response)"
		exit 1
		;;
esac

cd "$TEMPFOLDER/quake3"

mv 'DeFRaG Bundle all-in-one Linux 64bit' "$HOME/quake3"

wget 'https://cdn.playmorepromode.com/files/cpma-mappack-full.zip' -O cpma-mappack-full.zip
unzip cpma-mappack-full.zip -d "$HOME/quake3/baseq3/"
wget 'https://playmorepromode.com/files/latest/cpma' -O cpma-no-maps.zip
unzip cpma-no-maps.zip -d "$HOME/quake3"

cd "$HOME/quake3"

rm -fr "$TEMPFOLDER"

chmod a+x oDFe.vk.x64
chmod a+x oDFe.x64

# Quake Engine for CPMA
if true
then
	wget --content-disposition https://github.com/ec-/Quake3e/releases/download/latest/quake3e-linux-x86_64.zip
	unzip quake3e-linux-x86_64.zip
	chmod a+x quake3e*
	rm quake3e-linux-x86_64.zip
else
	# cnq3 has dropped non-Windows support :(
	git clone https://bitbucket.org/CPMADevs/cnq3.git

	cd cnq3

	case "${distro-}" in
		debian)
			sudo apt install nasm libsdl2-dev libglu1-mesa-dev
			;;
		*)
			echo "$0: unknown distro"
			exit 1
			;;
	esac

	sudo make clean client

	cp .bin/release_x64/cnq3-x64 ../cnq3-x64

	make clean
fi
