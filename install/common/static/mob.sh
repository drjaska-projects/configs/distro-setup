#!/bin/sh

set -eu

url="$(curl -sL https://api.github.com/repos/remotemobprogramming/mob/releases/latest \
	| grep "browser_download_url.*linux_amd64.tar.gz" \
	| cut -d ":" -f 2- | tr -d ' \"')"

echo "$url"

sudo url="$url" sh -c 'curl -L "$url" | tar xz -C /usr/local/bin mob --overwrite && chmod a+x /usr/local/bin/mob'
