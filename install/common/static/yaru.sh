#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed yaru-gtk-theme
		exit 0
		;;
esac

if [ -d "$HOME/programs/yaru/.git" ]
then
	cd "$HOME/programs/yaru"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/ubuntu/yaru
	cd yaru
fi

# build
meson "build" --prefix=/usr
# install
sudo ninja -C "build" install
