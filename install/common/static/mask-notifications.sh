#!/bin/sh

set -eu

# Mask notification dbus autostart services so that they don't conflict
# This allows installing several but starting only one of them by choice

# Dunst
if command -v dunst >/dev/null 2>&1
then
	sudo systemctl mask org.knopwob.dunst.service
fi

# KDE Plasma Notifications
if command -v plasma_waitforname >/dev/null 2>&1
then
	sudo systemctl mask org.kde.plasma.Notifications.service
fi
