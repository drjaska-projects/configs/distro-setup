#!/bin/sh

set -eu

# DEPRECATED in the favour of:
# https://gitlab.com/drjaska-projects/configs/zsh/-/blob/master/plugins/zsh-manpage-completion-generator/do-the-things.sh

exit 1

if [ -d "$HOME/programs/sh-manpage-completions/.git" ]
then
	cd "$HOME/programs/sh-manpage-completions"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/drjaska/sh-manpage-completions.git
	cd sh-manpage-completions
fi

find /usr/share/man/man1 -type f -exec bash -c '"$HOME/programs/sh-manpage-completions/run.sh" "${0}"' {} \;
find /usr/local/share/man/man1 -type f -exec bash -c '"$HOME/programs/sh-manpage-completions/run.sh" "${0}"' {} \;
