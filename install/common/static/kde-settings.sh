#!/bin/sh

set -eu

# TODO: look for a way which allows to set values via shell scripting
# open settings menus and click manually for now :(
kcmshell6 kcm_mouse # mouse acceleration
kcmshell6 kcm_screenlocker # lock screen after 15mins
kcmshell6 kcm_powerdevilprofilesconfig # dim screen and sleep
kcmshell6 kcm_kscreen # monitors

# Disable systemd based booting
# allows for example ~/.xinitrc to manage KDE's WM
kwriteconfig6 --file startkderc --group General --key systemdBoot false
