#!/bin/sh

set -eu

if [ -d "$HOME/xonotic/" ]
then
	XONOTICMOROSOPHOSCONFIGS="$HOME/xonotic/morosophos-config"
else
	XONOTICMOROSOPHOSCONFIGS="$HOME/xonotic-morosophos-config"
fi

if [ -d "$XONOTIC/.git" ]
then
	cd "$XONOTICMOROSOPHOSCONFIGS"
	git pull --autostash
else
	git clone https://gitlab.com/exe.pub/xonotic-cfg.git "$XONOTICMOROSOPHOSCONFIGS"
fi
