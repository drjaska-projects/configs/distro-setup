#!/bin/sh

set -eu

if [ -d "$HOME/xonotic/" ]
then
	XONOTICBASEDIR="$HOME/xonotic/0.8.6"
else
	XONOTICBASEDIR="$HOME/xonotic-0.8.6"
fi

if [ -e "$XONOTICBASEDIR" ]
then
	echo "$0: PANIC: $XONOTICBASEDIR exists already"
	exit 1
fi

TEMPDIR="$(mktemp -d)"

cd "$TEMPDIR"

wget https://dl.xonotic.org/xonotic-0.8.6.zip
unzip xonotic-0.8.6.zip

mv Xonotic "$XONOTICBASEDIR"
rm -fr "$TEMPDIR"
