#!/bin/sh

set -eu

if [ -d "$HOME/xonotic/" ]
then
	XONOTICBASEDIR="$HOME/xonotic/autobuild"
else
	XONOTICBASEDIR="$HOME/xonotic-autobuild"
fi

if [ -e "$XONOTICBASEDIR ]
then
	echo "$0: PANIC: $XONOTIC exists already"
	exit 1
fi

TEMPDIR="$(mktemp -d)"

cd "$TEMPDIR"

wget https://beta.xonotic.org/autobuild/Xonotic-latest.zip
unzip Xonotic-latest.zip

mv Xonotic "$XONOTICBASEDIR
rm -fr "$TEMPDIR"
