#!/bin/sh

set -eu

for icondir in $(sudo locate icon-theme.cache | sed 's#/icon-theme.cache$##')
do
	echo "$icondir"
	sudo gtk4-update-icon-cache -f "$icondir"
done
