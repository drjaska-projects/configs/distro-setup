#!/bin/sh

set -eu

cd "$HOME/programs"

if [ -d "diff-so-fancy/.git" ]
then
	cd diff-so-fancy
	git pull --autostash
else
	if [ -e "diff-so-fancy" ]
	then
		rm -fr diff-so-fancy
	fi
	git clone https://github.com/so-fancy/diff-so-fancy
fi

mkdir -p "$HOME/.local/bin/"
ln -srf "$HOME/programs/diff-so-fancy/diff-so-fancy" "$HOME/.local/bin/diff-so-fancy"
