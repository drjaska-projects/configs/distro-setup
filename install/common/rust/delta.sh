#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed git-delta
		exit 0
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

cargo install --locked --all-features --git https://github.com/dandavison/delta --branch master
