#!/bin/sh

set -eu

USEACL="${USEACL-false}"

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

# check that we have a config to run
if ! [ -e "/usr/local/etc/kbct/kbct.yaml" ]
then
	printf '\n\n%s\n\n\s\n\n' \
		"ERROR: KBCT REQUIRES A CONFIG TO RUN" \
		"PLEASE CREATE FILE /usr/local/etc/kbct/kbct.yaml"
	exit 1
fi

set +u
case "${distro-}" in
	debian)
		sudo apt-get update

		if [ "$USEACL" = "false" ]
		then
			sudo apt-get install -y libudev1 libudev-dev
		else
			sudo apt-get install -y libudev1 libudev-dev acl
		fi
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac
set -u

mkdir -p "$HOME/programs"
cd "$HOME/programs"

if ! [ -d "kbct" ]
then
	git clone https://github.com/samvel1024/kbct
	cd kbct
else
	cd kbct
	git pull --autostash
fi

cargo build --release

sudo mkdir -p /usr/local/bin/
sudo cp target/release/kbct /usr/local/bin/

# ensure that the user called keymapper exists
if [ -z "$(id keymapper)" ]
then
	sudo useradd keymapper
fi

# ensure that keymapper is in uinput group
if groups keymapper | grep -qv "uinput"
then
	sudo usermod -a -G uinput keymapper
fi

# udev rules for allowing keymapper to access uinput
sudo mkdir -p /etc/udev/rules.d/
if [ "$USEACL" = "false" ]
then
	cat <<-EOF | sudo tee /etc/udev/rules.d/90-keymapper.rules
	KERNEL=="uinput", GROUP="uinput", MODE="0660", OPTIONS+="static_node=uinput"
	KERNEL=="event[0-9]*", GROUP="uinput", MODE="0660"
	EOF
else
	cat <<-EOF | sudo tee /etc/udev/rules.d/90-keymapper.rules
	KERNEL=="event*", SUBSYSTEM=="input", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/input/%k"
	KERNEL=="uinput", SUBSYSTEM=="misc", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/uinput"
	EOF
fi

# load said udev rules on boot
sudo mkdir -p /etc/modules-load.d/
cat <<EOF | sudo tee /etc/modules-load.d/uinput.conf
uinput
EOF

sudo mkdir -p /usr/lib/systemd/system/
cat <<EOF | sudo tee /usr/lib/systemd/system/kbct.service
[Unit]
Description=kbct

[Service]
Type=simple
KillMode=process
WorkingDirectory=/
ExecStart=bash -c "/usr/local/bin/kbct remap --config /usr/local/etc/kbct/kbct.yaml"
Restart=on-failure
RestartSec=1
User=keymapper

[Install]
WantedBy=multi-user.target
EOF

# start kbct
# load uinput for this boot so that reboot isn't needed
sudo modprobe uinput
# load, enable and start the systemd service
sudo systemctl daemon-reload
#sudo systemctl enable kbct.service
#sudo systemctl start kbct.service
