#!/bin/sh

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

case "${distro-}" in
	debian)
		sudo apt-get update
		sudo apt-get install -y libdbus-1-dev libncursesw5-dev \
			libpulse-dev libssl-dev libxcb1-dev \
			libxcb-render0-dev libxcb-shape0-dev \
			libxcb-xfixes0-dev
		;;
	garuda|arch)
		sudo pacman -S --needed ncspot
		exit 0
		;;
	*)
		echo "$0: unsupported ncspot arch"
		exit 1
		;;
esac

if true
then
	cargo install --locked ncspot
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/hrkfdn/ncspot
	cd ncspot
	cargo build --release
fi
