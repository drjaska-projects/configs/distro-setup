#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed bacon
		exit 0
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

cargo install --locked bacon
