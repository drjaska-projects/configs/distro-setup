#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		# No Debian packages yet :/
		;;
	garuda|arch)
		sudo pacman -S --needed starship
		exit 0
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

if command -v cargo-binstall >/dev/null 2>&1
then
	cargo binstall --locked starship
else
	cargo install --locked starship
fi
