#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		# listed as dependencies but it compiles without?
		#sudo apt-get install -y libgit2 libcurl4-*-dev libssh-dev libssl-dev pkgconf
		;;
	garuda|arch)
		sudo pacman -S --needed cargo-update
		exit 0
		#sudo pacman -S --needed libgit2 curl libssh2 openssl pkgconf
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

if command -v cargo-binstall >/dev/null 2>&1
then
	cargo binstall --locked cargo-update
else
	cargo install --locked cargo-update
fi
