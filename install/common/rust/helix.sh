#!/bin/sh

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"


case "${distro-}" in
	debian)
		cargo install --locked --git https://github.com/helix-editor/helix helix-term
		;;
	garuda|arch)
		sudo pacman -S --needed helix
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac
