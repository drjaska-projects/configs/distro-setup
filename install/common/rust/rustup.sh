#!/bin/sh

set -eu

case "${distro-}" in
	debian)
		if command -v rustup >/dev/null 2>&1
		then
			rustup update
		else
			curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
		fi
		;;
	garuda)
		# no support, use other packages via pacman instead
		echo "$0: don't run this on Garuda"
		exit 1
		;;
	arch)
		# no support, use other packages via pacman instead
		echo "$0: don't run this on Arch"
		exit 1
		;;
	*)
		echo "$0: unknown distro"
		exit 1
		;;
esac
