#!/bin/sh

# shellcheck disable=SC2317

exit 1

# scrapped due to inability to have remapped keys participate
# in remapping more keys and due to hardcoded modifier keys
# https://github.com/samvel1024/kbct looks to be a straight upgrade

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

case "${distro-}" in
	debian)
		sudo apt-get update
		sudo apt-get install -y libevdev-dev pkg-config acl
		;;
	*)
		echo "$0: unsupported evremap arch"
		exit 1
		;;
esac

if [ -d "$HOME/programs/evremap/.git" ]
then
	cd "$HOME/programs/evremap"
	git pull --autostash
else
	mkdir -p "$HOME/programs"
	cd "$HOME/programs"
	git clone https://github.com/wez/evremap.git || true
	cd evremap
fi

cargo build --release

sudo mkdir -p /usr/local/bin/
sudo cp target/release/evremap /usr/local/bin/
sudo chown root:root /usr/local/bin/evremap
sudo chmod 755 /usr/local/bin/evremap

# ensure that keymapper user exists
if [ -z "$(id keymapper)" ]
then
	sudo useradd keymapper
	sudo usermod -a -G input keymapper
fi

sudo mkdir -p /etc/udev/rules.d/
cat <<EOF | sudo tee /etc/udev/rules.d/90-keymapper-acl.rules
KERNEL=="event*", SUBSYSTEM=="input", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/input/%k"
KERNEL=="uinput", SUBSYSTEM=="misc", RUN+="/usr/bin/setfacl -m user:keymapper:rw /dev/uinput"
EOF

sudo mkdir -p /usr/lib/systemd/system/
cat <<-EOF | sudo tee /usr/lib/systemd/system/evremap.service
	[Unit]
	Description=evremap

	[Service]
	Type=simple
	KillMode=process
	WorkingDirectory=/
	ExecStart=bash -c "/usr/local/bin/evremap remap /usr/local/etc/evremap/evremap.toml -d 0"
	Restart=on-failure
	RestartSec=1
	User=keymapper
	Group=input

	[Install]
	WantedBy=multi-user.target
EOF

# don't use the original "run as root" service but the above one
#sudo cp evremap.service /usr/lib/systemd/system/evremap.service

# FIXME: don't use preset config
cat <<-EOF | sudo tee /usr/local/etc/evremap/evremap.toml
	# The name of the device to remap.
	# Run \`sudo evremap list-devices\` to see the devices available
	# on your system.
	device_name = "AT Translated Set 2 keyboard"

	# If you have multiple devices with the same name, you can optionally
	# specify the \`phys\` value that is printed by the \`list-devices\` subcommand
	# phys = "usb-0000:07:00.3-2.1.1/input0"

	# Configure CAPSLOCK as a Dual Role key.
	# Holding it produces LEFTCTRL, but tapping it
	# will produce ESC.
	# Both \`tap\` and \`hold\` can expand to multiple output keys.
	[[dual_role]]
	input = "KEY_CAPSLOCK"
	hold = ["KEY_LEFTCTRL"]
	tap = ["KEY_ESC"]
EOF

sudo systemctl daemon-reload
sudo systemctl enable evremap.service
sudo systemctl start evremap.service
