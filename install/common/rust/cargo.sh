#!/bin/sh

set -eu

if ! command -v cargo >/dev/null 2>&1
then
	case "${distro-}" in
		debian)
			"$HOME/.config/distro-setup/install/common/rust/rustup.sh"

			# could not find best practices for selecting which cargo/env to source
			# assume default if inherited PATH doesn't contain cargo executable
			[ -e "$HOME/.cargo/env" ] && \
				. "$HOME/.cargo/env"
			[ -e "${XDG_DATA_HOME-$HOME/.local/share}/cargo/env" ] && \
				. "${XDG_DATA_HOME-$HOME/.local/share}/cargo/env"
			;;
		garuda|arch)
			sudo pacman -S --needed rust cargo
			;;
	esac

	if ! command -v cargo >/dev/null 2>&1
	then
		exit 1
	fi
fi
