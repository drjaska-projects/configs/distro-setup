#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed sad
		exit 0
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

cargo install --locked --all-features --git https://github.com/ms-jpq/sad --branch senpai
