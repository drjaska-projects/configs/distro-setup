#!/bin/sh

set -eu

case "${distro-}" in
	garuda|arch)
		sudo pacman -S --needed cargo-binstall
		exit 0
		;;
esac

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

if command -v cargo-binstall >/dev/null 2>&1
then
       cargo binstall --locked cargo-binstall
else
       cargo install --locked cargo-binstall
fi
