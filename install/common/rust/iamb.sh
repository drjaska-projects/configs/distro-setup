#!/bin/sh

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

nightly="${nightly-false}"

if [ "$nightly" != "false" ]
then
	cargo install --locked --git https://github.com/ulyssa/iamb
else
	cargo install --locked iamb
fi
