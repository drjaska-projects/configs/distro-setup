#!/bin/sh

set -eu

. "$HOME/.config/distro-setup/install/common/rust/cargo.sh"

cargo install --locked mprocs
