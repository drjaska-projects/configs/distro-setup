#!/bin/bash

set -eu

mkdir -p "$HOME/.config/"

distro="$(grep '^ID=' /etc/os-release | cut -d '=' -f 2)"
case "$distro" in
	debian)
		# supported
		;;
	garuda)
		# supported
		;;
	arch)
		# supported
		;;
	*)
		echo "Setup-info: unsupported distro"
		exit 1
		;;
esac
export distro

# why are there systems without git :(
if ! [ -x "$(command -v git)" ]
then
	echo "Setup-info: tfw no git, can't function without it"
	echo "Setup-info: attempting to install git"
	case "$distro" in
		debian)
			sudo apt-get update
			sudo apt-get install -y git
			;;
		garuda|arch)
			sudo pacman -S --needed git
			;;
		*)
			echo "Setup-info: unable to determinate distro for git installation"
			exit 1
			;;
	esac
fi

if [ -d "$HOME/.config/distro-setup/.git" ]
then
	echo "Setup-info: distro-setup has been cloned already, skip cloning, checking for updates"
	cd "$HOME/.config/distro-setup/"
	git status
	git pull --autostash
else
	echo "Setup-info: cloning distro-setup"
	git clone https://gitlab.com/drjaska-projects/configs/distro-setup.git "$HOME/.config/distro-setup"
	cd "$HOME/.config/distro-setup/"
fi

case "$distro" in
	debian)
		echo "Setup-info: cloning config files from git repos"
		./install/git-configs.sh

		echo "Setup-info: installing apt-get packages from Debian repositories"
		. ./install/deb/debian-packages.sh

		echo "Setup-info: disabling mouse acceleration"
		./install/xorg/disable-mouse-accel.sh

		echo "Setup-info: setting up Xorg keyboard"
		./install/xorg/keyboard.sh

		echo "Setup-info: installing programs"
		./install/deb/programs.sh

		echo "Setup-info: cloning Yaru theme"
		./install/common/static/yaru.sh

		echo "Setup-info: Install script ran successfully. Finish the install with rebooting after sudo apt-get install qjackctl"

		exit 0 ;;
	garuda)
		echo "Setup-info: cloning config files from git repos"
		./install/git-configs.sh

		echo "Setup-info: disabling Xorg mouse acceleration"
		./install/xorg/disable-mouse-accel.sh

		echo "Setup-info: setting up Xorg keyboard"
		./install/xorg/keyboard.sh

		echo "Setup-info: installing packages from Arch / Garuda repositories"
		./install/arch/garuda-packages.sh

		echo "Setup-info: installing programs"
		./install/arch/programs.sh

		exit 0 ;;
	arch)
		echo "Setup-info: cloning config files from git repos"
		./install/git-configs.sh

		echo "Setup-info: disabling Xorg mouse acceleration"
		./install/xorg/disable-mouse-accel.sh

		echo "Setup-info: setting up Xorg keyboard"
		./install/xorg/keyboard.sh

		echo "Setup-info: installing packages from Arch repositories"
		./install/arch/arch-packages.sh

		echo "Setup-info: installing programs"
		./install/arch/programs.sh

		exit 0 ;;
	*)
		echo "Setup-info: No valid distros detected"
		exit 1 ;;
esac
