#!/bin/bash

set -eu

mkdir -p "$HOME/.config/"
cd "$HOME/.config/"

clonerepo() {
	if ! [ -d "$2" ]
	then
		git clone "$1"
	fi
}

# clone    git remote URL                                                  where in ~/.config/
clonerepo https://gitlab.com/drjaska-projects/configs/cargo.git           cargo
clonerepo https://gitlab.com/drjaska-projects/configs/dunst.git           dunst
clonerepo https://gitlab.com/drjaska-projects/configs/feh.git             feh
clonerepo https://gitlab.com/drjaska-projects/configs/fonts.git           fonts
clonerepo https://gitlab.com/drjaska-projects/configs/git.git             git
clonerepo https://gitlab.com/drjaska-projects/configs/iamb.git            iamb
clonerepo https://gitlab.com/drjaska-projects/configs/gnome-terminal.git  gnome-terminal
clonerepo https://gitlab.com/drjaska-projects/configs/gtk-themes.git      gtk-themes
clonerepo https://gitlab.com/drjaska-projects/configs/homefolder.git      homefolder
clonerepo https://gitlab.com/drjaska-projects/configs/keyszer.git         keyszer
clonerepo https://gitlab.com/drjaska-projects/configs/kitty.git           kitty
clonerepo https://gitlab.com/drjaska-projects/configs/ncspot.git          ncspot
clonerepo https://gitlab.com/drjaska-projects/configs/pipewire.git        pipewire
clonerepo https://gitlab.com/drjaska-projects/configs/tmux.git            tmux
clonerepo https://gitlab.com/drjaska-projects/configs/xmobar.git          xmobar
#clonerepo https://gitlab.com/drjaska-projects/configs/xmonad.git          xmonad
clonerepo https://gitlab.com/drjaska-projects/configs/zsh.git             zsh

touch "$HOME/.config/xmobar/.xmobarrc-"{0,1,2,3} # out of quotes for bashism

"$HOME/.config/distro-setup/install/hookup-all-configs.sh"

"$HOME/.config/zsh/plugins/install-plugins.sh"
