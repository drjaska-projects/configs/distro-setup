#!/bin/sh

# don't exit on error on purpose so that next
# repositories are checked even if one is missing/fails

"$HOME/.config/cargo/sync-configs.sh" && \
printf "\033[36m--- cargo configs synced ---\033[0m\n" ||
printf "\033[31m--- cargo config sync errored, most likely not found ---\033[0m\n"

dconf load /org/gnome/terminal/ < "$HOME/.config/gnome-terminal/preferences.txt" && \
printf "\033[36m--- gnome-terminal configs synced ---\033[0m\n" ||
printf "\033[31m--- gnome-terminal config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/fonts/sync-configs.sh" && \
printf "\033[36m--- font configs synced ---\033[0m\n" ||
printf "\033[31m--- font config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/gtk-themes/sync-configs.sh" && \
printf "\033[36m--- gtk configs synced ---\033[0m\n" ||
printf "\033[31m--- gtk config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/homefolder/sync-configs.sh" && \
printf "\033[36m--- homefolder configs synced ---\033[0m\n" ||
printf "\033[31m--- homefolder config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/keyszer/sync-configs.sh" && \
printf "\033[36m--- keyszer configs synced ---\033[0m\n" ||
printf "\033[31m--- keyszer config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/xmobar/sync-configs.sh" && \
printf "\033[36m--- xmobar configs synced ---\033[0m\n" ||
printf "\033[31m--- xmobar config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/xmonad/sync-configs.sh" && \
printf "\033[36m--- xmonad configs synced ---\033[0m\n" ||
printf "\033[31m--- xmonad config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/zsh/sync-configs.sh" && \
printf "\033[36m--- zsh configs synced ---\033[0m\n" ||
printf "\033[31m--- zsh config sync errored, most likely not found ---\033[0m\n"

exit 0
