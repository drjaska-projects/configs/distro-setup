#!/bin/sh

set -eux



##############################
# Manual notes, script below #
##############################



# Mount btrfs partition's root
# The swap file should not be on a COW subvolume
#
#sudo mount --mkdir /dev/mydevice /mnt/btrfs

# Create btrfs swap file in btrfs root
#
#sudo btrfs filesystem mkswapfile --size 5G /mnt/btrfs/swap

# Enable swap file
#
#sudo swapon /mnt/btrfs/swap

# Check that swap got enabled
#
# cat /proc/swaps

# Add btrfs root and swapfile to fstab
# Enables swap on future boots
#
# UUID=MYUUID /mnt/btrfs btrfs defaults,noatime,compress=zstd,subvol=/ 0 0
# /mnt/btrfs/swap none swap defaults 0 0
#
# Relies on btrfs partition root being mounted before swap file

# Swap file done

# Enable hibernation for GRUB

# Get resume_offset
#
#sudo btrfs inspect-internal map-swapfile -r /mnt/btrfs/swap

# Add 'resume=/dev/mydevice resume_offset=$resume_offset'
# to line GRUB_CMDLINE_LINUX_DEFAULT
# in /etc/default/grub
# valid example:
#
# GRUB_CMDLINE_LINUX_DEFAULT="quiet resume=/dev/mapper/paska resume_offset=$resume_offset"

# Add resume_offset to live kernel settings via /sys/power/resume_offset
#
#sudo sh -c 'btrfs inspect-internal map-swapfile -r /mnt/btrfs/swap > /sys/power/resume_offset'

# Update grub
#
#sudo update-grub
#
# above wrapper or below manually without the wrapper
#
#sudo grub-mkconfig -o /boot/grub/grub.cfg

# Hibernation setup done



####################
# Automatic Script #
####################



# Get the device which is mounted as the root filesystem
ROOTDRIVE="$(findmnt -n -t btrfs --list | grep -m 1 -P "^/ .*subvol=/(@|@rootfs)$" | awk '{print $2}')"
ROOTDRIVE="${ROOTDRIVE%\[/@rootfs\]}" # remove [/@rootfs] subvolume part
ROOTDRIVE="${ROOTDRIVE%\[/@\]}" # remove [/@] subvolume part

# Get the device which is mounted at /mnt/btrfs
MOUNTEDRIVE="$(findmnt -n -t btrfs --list | grep -m 1 "^/mnt/btrfs .*subvol=/$" | awk '{print $2}')"

# Check that we have a valid root now
if [ -z "${ROOTDRIVE-}" ]
then
	echo "$0: is your / not btrfs..?"
	exit 1
fi

# Check that we got a device mounted on /mnt/btrfs
if [ -z "${MOUNTEDRIVE-}" ]
then
	echo "$0: btrfs root not mounted on /mnt/btrfs"
	echo "$0: try 'sudo mount --mkdir $ROOTDRIVE /mnt/btrfs'"
	exit 2
fi

# Create btrfs swap file in btrfs root
sudo btrfs filesystem mkswapfile --size 5G /mnt/btrfs/swap

# Enable swap file
sudo swapon /mnt/btrfs/swap

# Check that swap got enabled
if ! grep -q "^/mnt/btrfs/swap" /proc/swaps
then
	echo "$0: did not succeed in enabling the swap file?"
	exit 3
fi

ROOTDRIVEUUID="$(sudo blkid | grep "$ROOTDRIVE" | sed -r 's#.* UUID="([^ ]+)" .*#\1#')"

# Add btrfs root and swapfile to fstab
# Enables swap on future boots
if ! grep " /mnt/btrfs " /etc/fstab
then
	cat <<-EOF | sudo tee -a /etc/fstab
		"UUID=\"$ROOTDRIVEUUID\"" /mnt/btrfs btrfs defaults,noatime,compress=zstd,subvol=/ 0 0
	EOF
fi
if ! grep "^/mnt/btrfs/swap " /etc/fstab
then
	cat <<-EOF | sudo tee -a /etc/fstab
		/mnt/btrfs/swap none swap defaults 0 2
	EOF
fi

# Get resume offset
RESUME_OFFSET="$(sudo btrfs inspect-internal map-swapfile -r /mnt/btrfs/swap)"

# Add resume offset to live kernel settings via /sys/power/resume_offset
sudo sh -c 'btrfs inspect-internal map-swapfile -r /mnt/btrfs/swap > /sys/power/resume_offset'

# Comment out the old line
# Copy it and add 'resume=/dev/mydevice resume_offset=$resume_offset'
sudo sed -ri "s#GRUB_CMDLINE_LINUX_DEFAULT=\"(.*)\"\$#\#GRUB_CMDLINE_LINUX_DEFAULT=\"\1\"\nGRUB_CMDLINE_LINUX_DEFAULT=\"\1 resume=/dev/disk/by-uuid/${ROOTDRIVEUUID} resume_offset=${RESUME_OFFSET}\"#" /etc/default/grub

# Update grub
if command -v update-grub >/dev/null 2>&1
then
	sudo update-grub
else
	sudo grub-mkconfig -o /boot/grub/grub.cfg
fi
