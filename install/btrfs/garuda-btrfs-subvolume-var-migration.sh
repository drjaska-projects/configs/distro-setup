#!/bin/sh

set -eux

cd /mnt/btrfs
# Rename subvolumes
sudo mv @      @rootfs
sudo mv @cache @var_cache
sudo mv @log   @var_log
sudo mv @tmp   @var_tmp

# Update fstab with ^
# NOTE: does not support subvol=/@foo,othermountoptionhere
sudo sed \
	-e 's#subvol=/@ #subvol=/@rootfs #' \
	-e 's#subvol=/@cache #subvol=/@var_cache #' \
	-e 's#subvol=/@log #subvol=/@var_log #' \
	-e 's#subvol=/@tmp #subvol=/@var_tmp #' \
	-i /etc/fstab

# Create snapper configs
sudo snapper -c @home      create-config /home
sudo snapper -c @root      create-config /root
#sudo snapper -c @srv      create-config /srv # empty for now
sudo snapper -c @var_tmp   create-config /var/tmp
sudo snapper -c @var_cache create-config /var/cache
sudo snapper -c @var_log   create-config /var/log

# Update initramfs
# Garuda defaults to dracut
if command -v dracut >/dev/null 2>&1
then
	sudo dracut-rebuild

elif command -v mkinitcpio >/dev/null 2>&1
then
	sudo mkinitcpio --allpresets
else
	echo "$0: could not find initramfs provider"
	exit 1
fi

# (re)Install grub
if [ -e /sys/firmware/efi/efivars/ ]
then
	sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi
else
	sudo grub-install --target=i386-pc /dev/sda
fi

# Update grub
if command -v update-grub >/dev/null 2>&1
then
	sudo update-grub
else
	sudo grub-mkconfig -o /boot/grub/grub.cfg
fi
