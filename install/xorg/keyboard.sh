#!/bin/sh

set -eu

# I could also set the option "nbsp:none" but I haven't
# been able to compose "<Multi_key>" composes with
# this setup, otherwise <rctrl><space><space> would
# type a nobreakspace character or many other similar
# characters which I do not like. As I can't type them
# with this setup the option wouldn't make any difference
# as the option would likely be overridden anyways with
# when changing this config at all. It's likely that
# nodeadkeys variant disables the multi_key composes
# but I am not certain.

sudo mkdir -p /etc/X11/xorg.conf.d/
cat <<EOF | sudo tee /etc/X11/xorg.conf.d/10-keyboard.conf
Section "InputClass"
	Identifier "system-keyboard"
	MatchIsKeyboard "on"
	Option "XkbLayout" "fi"
	Option "XkbVariant" "nodeadkeys"
	Option "XkbModel" "pc105"
	Option "XkbOptions" "compose:rctrl,keypad:pointerkeys"
EndSection
EOF
