#!/bin/sh -eu

# Option "metamodes" "nvidia-auto-select +0+0 { ForceCompositionPipeline = On }"
# causes a change on what monitor my mouse is when starting X11..?
# Screen identifier is ignored if set and all monitors get composition?
#
# ~/bin/force-composition-pipeline.sh preferred over this for now

# shellcheck disable=SC2317

echo "no"
exit 69

sudo mkdir -p /etc/X11/xorg.conf.d/
cat <<EOF | sudo tee /etc/X11/xorg.conf.d/70-nvidia-screen-tearing.conf
Section "Device"
	Identifier "NVIDIA Card"
	Driver     "nvidia"
	VendorName "NVIDIA Corporation"
EndSection

Section "Screen"
	Identifier "*"
	Option     "metamodes" "nvidia-auto-select +0+0 { ForceCompositionPipeline = On }"
	Option     "AllowIndirectGLXProtocol" "off"
	Option     "TripleBuffer" "on"
EndSection
EOF
