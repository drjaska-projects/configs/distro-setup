#!/bin/sh

set -eu

sudo mkdir -p /usr/share/X11/xorg.conf.d/
cat <<EOF | sudo tee /usr/share/X11/xorg.conf.d/50-mtrack.conf
Section "InputClass"
	MatchIsTouchpad "true"
	Identifier      "Multitouch Touchpad"
	Driver          "mtrack"
#	Driver          "libinput"
# libinput doesn't work as well for gestures,
# better for no gestures I think?
		Option "Sensitivity"          "0.25"
		Option "DisableWhileTyping"   "0"
		Option "DisableOnPalm"        "true"
		Option "BottomEdge"           "10"
		Option "Tapping"              "on"
		Option "TapButton2"           "2"
		Option "TapButton3"           "3"
		Option "EmulateTwoFingerMinZ" "40"
		Option "EmulateTwoFingerMinW" "8"
		Option "CoastingSpeed"        "0"
		Option "FingerLow"            "5"
		Option "FingerHigh"           "50"
		Option "MaxTapTime"           "125"
EndSection
EOF
