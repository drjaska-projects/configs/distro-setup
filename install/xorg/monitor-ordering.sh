#!/bin/sh

set -eu

sudo mkdir -p /etc/X11/xorg.conf.d/

cat <<EOF | sudo tee /etc/X11/xorg.conf.d/50-GTX1070-monitor-order.conf
Section "Device"
  Identifier "GeForce GTX 1070"
  Driver "nvidia"
  Option "nvidiaXineramaInfoOrder" "DVI-D-0, DP-0, DP-3, HDMI-0"
EndSection

Section "Device"
  Identifier "GeForce GTX 1070 Ti"
  Driver "nvidia"
  Option "nvidiaXineramaInfoOrder" "DP1 DVI-D-0"
EndSection
EOF
