#!/bin/sh

set -eu

mkdir -p "$HOME/.config/"
cd "$HOME/.config/"

export distro="termux"

# why are there systems without git :(
if ! [ -x "$(command -v git)" ]
then
	echo "Setup-info: tfw no git, can't function without it"
	echo "Setup-info: attempting to install git"

	apt-get install -y git
fi

if [ -d "$HOME/.config/distro-setup/.git" ]
then
	echo "Setup-info: distro-setup has been cloned already, skip cloning, checking for updates"
	cd "$HOME/.config/distro-setup/"
	git status
	git pull --autostash
else
	echo "Setup-info: cloning distro-setup"
	git clone https://gitlab.com/drjaska-projects/configs/distro-setup.git "$HOME/.config/distro-setup"
	cd "$HOME/.config/distro-setup/"
fi

echo "Setup-info: cloning config files from git repos"
"$HOME/.config/distro-setup/install/termux/termux-git-configs.sh"
