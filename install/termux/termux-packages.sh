#!/bin/bash

set -eu

# TRAILING - REMOVES THAT PACKAGE

declare -a PACKAGES
PACKAGES+=(
	# file receiving/sending
	curl
	wget
	rsync
	git
		git-delta
		diff-so-fancy
	netcat-openbsd
	gnupg

	openssh
	mosh

	# package manager
	apt
		apt-file

	dos2unix

	# compression
	zip unzip
	tar

	# python things
	python
	python-pip

	# shells and interactivity
	zsh
		zsh-completions
	fish
	starship
	autojump
	grep
	ripgrep
	#xclip
	#xsel
	fzf
	#retry
	bat
	tealdeer
	screen tmux
	command-not-found
	reptyr
	whois
	traceroute
	nmap
	fd
	entr
	psmisc
	pv
	moreutils # sponge
	which
	#man-db

	# tools
	tree
	jq

	# system information
	neofetch
	fastfetch
	procps # top
	htop
	ncdu

	# editor stuff
	#vim-gtk3
	neovim
		python-pynvim
		shellcheck

	# converters
	imagemagick
	ffmpeg

	# video/audio players
	vlc
	mpv
		#mpv-mpris
	#playerctl

	# media
	#yt-dlp
	#mkchromecast
	#mkchromecast-alsa
)

yes | apt-get update
yes | apt-get upgrade
apt-get install -y "${PACKAGES[@]}"

# update tldr pages
tldr -u

# update default shell
chsh -s "$(command -v zsh)"

mkdir -p ~/.local/bin
