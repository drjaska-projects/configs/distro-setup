#!/bin/sh

set -eu

mkdir -p "$HOME/.config/"
cd "$HOME/.config/"

clonerepo() {
	if ! [ -d "$2" ]
	then
		git clone "$1"
	fi
}

# clone    git remote URL                                                  where in ~/.config/
clonerepo https://gitlab.com/drjaska-projects/configs/git.git             git
clonerepo https://gitlab.com/drjaska-projects/configs/homefolder.git      homefolder
clonerepo https://gitlab.com/drjaska-projects/configs/tmux.git            tmux
clonerepo https://gitlab.com/drjaska-projects/configs/zsh.git             zsh

"$HOME/.config/zsh/plugins/install-plugins.sh"
