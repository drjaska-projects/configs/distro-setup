#!/bin/sh

set -eu

mkdir -p "$HOME/.config/"
cd "$HOME/.config/"

distro="$(grep '^ID=' /etc/os-release | cut -d '=' -f 2)"
case "$distro" in
	debian)
		# supported
		;;
	*)
		echo "Setup-info: unsupported distro"
		exit 1
		;;
esac
export distro

# why are there systems without git :(
if ! [ -x "$(command -v git)" ]
then
	echo "Setup-info: tfw no git, can't function without it"
	echo "Setup-info: attempting to install git"
	case "$distro" in
		debian)
			sudo apt-get update
			sudo apt-get install -y git
			;;
		*)
			echo "Setup-info: unable to determinate distro for git installation"
			exit 1
			;;
	esac
fi

if [ -d 'distro-setup/.git' ]
then
	echo "Setup-info: distro-setup has been cloned already, skip cloning, checking for updates"
	cd "$HOME/.config/distro-setup/"
	git status
	git pull --autostash
else
	echo "Setup-info: cloning distro-setup"
	git clone https://gitlab.com/drjaska-projects/configs/distro-setup.git "$HOME/.config/distro-setup"
	cd "$HOME/.config/distro-setup/"
fi

case "$distro" in
	debian|raspbian)
		# does this fuck up Raspbian?
		#echo "Setup-info: switching to Debian Stable repositories"
		#. "$HOME/.config/distro-setup/install/deb/debian-stable-repo.sh"

		echo "Setup-info: cloning config files from git repos"
		./install/server-git-configs.sh

		echo "Setup-info: installing apt-get packages from Debian repositories"
		./install/deb/debian-server-packages.sh

		echo "Setup-info: installing programs"
		./install/deb/debian-server-programs.sh

		echo "Setup-info: hooking up configs"
		./install/server-hookup-configs.sh

		echo "Setup-info: install script ran successfully"
		exit 0 ;;

	*)
		echo "Setup-info: no valid distros detected"
		exit 1 ;;
esac
