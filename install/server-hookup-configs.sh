#!/bin/sh

# shebang doesn't include -eu on purpose so that next
# repositories are checked even if one is missing

"$HOME/.config/homefolder/sync-configs.sh" && \
printf "\033[36m--- homefolder configs synced ---\033[0m\n" ||
printf "\033[31m--- homeflder config sync errored, most likely not found ---\033[0m\n"

"$HOME/.config/zsh/sync-configs.sh" && \
printf "\033[36m--- zsh configs synced ---\033[0m\n" ||
printf "\033[31m--- zsh config sync errored, most likely not found ---\033[0m\n"

return 0
