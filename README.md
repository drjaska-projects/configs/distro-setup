# Dr. Jaska's distro-setup script collection

This script collection installs all the applications and settings
for me after install a new OS from a Live USB making my life
easier if my Linux setup explodes.

While snapshots can be considered as an alternative they can not
rebuild a system from ground up, they can only restore up to a point.
If after an update there is a new infrastructure conflict or anything
similar the problems due to old infrastructure are bound to re-emerge
after every update on each restoration. With snapshots the past can not
be abandoned which is why I prefer rebuilding my setup and storing
personal data like photo albums on multiple drives
and mostly disconnected from my PC.

Another upside with this collection compared to snapshots is that I'll
never have to struggle with remembering how I managed to install that
one problematic software again with a not so straight forward install
process as I've automated it into a script on the first install.

I'm not personally a fan of becoming attached to a specific
installation without any rebuilding consideration, a single blow which
kills it beyond easy repair and your productivity is handicapped for
days if not even weeks or months due to having to manually rebuild
it from memory or make do without your desired setup.

# Manual Installation:

Follow the installation scripts branching out from
install/everything.sh and install what you please.

Supported GNU/Linux distributions:
  - Debian (Stable / Testing)
  - Raspberry Pi Debian (Stable / Testing)
  - Garuda
  - Arch

## Full blind installation

Without pre-cloning:

```sh
wget -O- https://gitlab.com/drjaska-projects/configs/distro-setup/-/raw/master/install/everything.sh | sh
```

or

```sh
curl -sSL https://gitlab.com/drjaska-projects/configs/distro-setup/-/raw/master/install/everything.sh | sh
```

With pre-cloning:

```sh
# install git
sudo apt install -y git
sudo pacman -S --needed git

cd ~/.config/
git clone https://gitlab.com/drjaska-projects/configs/distro-setup.git
~/.config/distro-setup/install/everything.sh
```
